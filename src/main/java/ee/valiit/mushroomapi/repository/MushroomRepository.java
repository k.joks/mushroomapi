package ee.valiit.mushroomapi.repository;
import ee.valiit.mushroomapi.model.Mushroom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MushroomRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Mushroom> fetchMushroom() {
        List<Mushroom> seened = jdbcTemplate.query("SELECT * FROM seentetabel", (row, rowNum) -> {
                    return new Mushroom(
                            row.getInt("id"),
                            row.getString("nameEe"),
                            row.getString("nameLd"),
                            row.getString("family"),
                            row.getString("picture"),
                            row.getString("color"),
                            row.getString("body"),
                            row.getString("hat"),
                            row.getString("meat"),
                            row.getString("petals"),
                            row.getString("habitat"),
                            row.getString("harvest"),
                            row.getString("preparation"),
                            row.getString("preservation")

                    );
                }
        );
        return seened;
    }

    public Mushroom fetchMushroom(int id) {
        List<Mushroom> seened = jdbcTemplate.query("SELECT * FROM seentetabel WHERE id = ?",
                new Object[]{id},
                (row, rowNum) -> {
                    return new Mushroom(
                            row.getInt("id"),
                            row.getString("nameEe"),
                            row.getString("nameLd"),
                            row.getString("family"),
                            row.getString("picture"),
                            row.getString("color"),
                            row.getString("body"),
                            row.getString("hat"),
                            row.getString("meat"),
                            row.getString("petals"),
                            row.getString("habitat"),
                            row.getString("harvest"),
                            row.getString("preparation"),
                            row.getString("preservation")
                    );
                }
                );
        if(seened.size()>0) {
            return seened.get(0);
        }else{
            return null;
        }
    }
//    public void deleteMushroom(int id) {
//        jdbcTemplate.update("DELETE FROM seentetabel WHERE id = ?", id);
//    }
    public void addMushroom(Mushroom mushroom) {
        jdbcTemplate.update("INSERT INTO seentetabel (nameEe, nameLd, family, picture, color, body, hat, meat, petals, habitat, harvest, preparation, preservation) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                mushroom.getNameEe(), mushroom.getNameLd(), mushroom.getFamily(), mushroom.getPicture(), mushroom.getColor(), mushroom.getBody(),
                mushroom.getHat(), mushroom.getMeat(), mushroom.getPetals(), mushroom.getHabitat(), mushroom.getHarvest(), mushroom.getPreparation(), mushroom.getPreservation());
    }
    public void updateMushroom(Mushroom mushroom) {
        jdbcTemplate.update("UPDATE seentetabel SET nameEe = ?, nameLd = ?, family = ?, picture = ?, color = ?, body = ?, hat = ?, meat = ?, petals = ?, habitat = ?, harvest = ?," +
                        "preparation = ?, preservation = ? WHERE id = ?",
                mushroom.getNameEe(), mushroom.getNameLd(), mushroom.getFamily(), mushroom.getPicture(), mushroom.getColor(), mushroom.getBody(),
                mushroom.getHat(), mushroom.getMeat(), mushroom.getPetals(), mushroom.getHabitat(), mushroom.getHarvest(), mushroom.getPreparation(), mushroom.getPreservation(), mushroom.getId());

    }
}
