package ee.valiit.mushroomapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MushroomapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MushroomapiApplication.class, args);
	}

}
